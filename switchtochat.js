

        Hooks.once('init', () => {
            
            game.settings.register("switch-to-chat", "active", {
                name: "Show chat when chatting or rolling",
                hint: "Show the chat tab whenever you chat or roll",
                scope: "client",
                config: true,
                type: Boolean,
                default: true
            });

            game.settings.register("switch-to-chat", "everyone", {
                name: "On everyone's roll",
                hint: "Show the chat on everyone's roll. If off it only shows for your rolls",
                scope: "client",
                config: true,
                type: Boolean,
                default: false
            });
        })



        Hooks.on('createChatMessage', (chatMessage) => {
            if(ui.sidebar.activeTab == "chat") return;
            

            let active = game.settings.get("switch-to-chat", "active");
            if(!active) return;

            let chatVisiblshowChatOnCombatTrackerTabeInCombat = false;
            if (game.modules.has("foundry-combat-focus")) {
                chatVisiblshowChatOnCombatTrackerTabeInCombat = game.settings.get("foundry-combat-focus", "showChatOnCombatTrackerTab") && game.modules.get("foundry-combat-focus").active;
            }

            let everyone = game.settings.get("switch-to-chat", "everyone");
            let IsAuthor = chatMessage.isAuthor
            let isContentVisible = chatMessage.isContentVisible

            
            // Escape clauses
            if (!IsAuthor && !chatMessage.isRoll && !chatMessage.data.flags["midi-qol"] ) return;
            if((!everyone && !IsAuthor ) || !isContentVisible) return;
            if(ui.sidebar.activeTab == "combat" && chatVisiblshowChatOnCombatTrackerTabeInCombat) return;


            ui.sidebar.activateTab("chat");
        })
    

        
    

