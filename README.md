# README #

### What is this module for? ###
Player: "I want shoot it's eye with my crossbow"  
DM: "Sure thing - Make your attack roll"  
Player: "... ok, I clicked on the crossbow, now what...?"  
DM: "Go to the chat tab"  
Player: "... Where's that again?"  
DM: *sigh* "Side bar on the left - click the first tab, the speech bubble"  
Player: "... ah got it"  
The dice finally roll.  
  
  
Switch to Chat simply switches the sidebar to the chat tab whenever you chat or make a roll.  
Optionally you can set it switch to the chat whenever anybody rolls so you can see the result.  
The options to turn on and off are available to each player in their settings.  


### License ###
MIT

